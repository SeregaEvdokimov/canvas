let keys = {
    'W': 87,
    'S': 83,
    'A': 65,
    'D': 68,
    'LEFT': 37,
    'RIGHT': 39,
};

let keyDown = {};

let setKey = function(keyCode) {
    keyDown[keyCode] = true;
};

let clearKey = function (keyCode) {
    keyDown[keyCode] = false;
};

let isKeyDown = function(keyName) {
    return keyDown[keys[keyName]] === true;
};

let isAnyKeyDown = function() {
    for(let key in keyDown) {
        if(keyDown[key]) {
             return true;
        }
    }
};


window.addEventListener('load', function() {

    window.addEventListener('keydown', function (e) {
        // console.log('keydown');
        setKey(e.keyCode);
    });

    window.addEventListener('keyup', function (e) {
        // console.log('keyup');
        clearKey(e.keyCode);
    });

});