let grid = {
    nodes: [],

    add: function(x, y, w, h, color) {
        this.nodes.push(new _Enemy(x, y, w, h, color));
    },

    generate: function(count, w, h, color) {
        let dw = 5, dx = dw, dy = dw,
            dCountX = Math.ceil(width / (w + dw)) - 1,
            dAllW = Math.ceil((width - (w + dw) * dCountX) / 2);

        dy = dAllW;

        for(let i = 0; i < count; i++) {
            for(let j = 0; j < dCountX; j++ ) {
                if(j === 0) {
                    dx += Math.ceil(dAllW - dw / 2);
                }

                this.add(dx, dy, w, h, color);
                dx += w + dw;
            }
            dy += h + dw;
            dx = dw;
        }
    },

    create: function(map) {
        let dOffsetX = (width - (map.tails[0].length * (map.width + map.offset))) / 2;
        for(let row in map.tails) {
            for(let item in map.tails[row]) {
                let tile = map.tails[row][item];

                let dx = dOffsetX + item * (map.width + map.offset);
                let dy = map.offset + row * (map.height + map.offset);

                if(tile === 1) {
                    this.add(dx, dy, map.width, map.height, map.color);
                }
            }
        }
    },

    destroy: function(id) {
        this.nodes.splice(id, 1);
    },

    draw: function() {
        for(let index in this.nodes) {
            this.nodes[index].draw();
        }
    },

    clear: function() {
        this.nodes = [];
    }
};

let _Enemy = function (x, y, w, h, color) {
    this.x = x;
    this.y = y;
    this.width = w;
    this.height = h;
    this.color = color;
};

_Enemy.prototype.draw = function () {
    drawRect(this.x, this.y, this.width, this.height, this.color);
};