let canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d');

canvas.width = canvas.scrollWidth;
canvas.height = canvas.scrollHeight;

let Animation = function() {
    this.rectangles = [
        {x: 0, y:   0, width: 100, height: 100, color: 'red',   speedX:  60},
        {x: 0, y: 100, width: 100, height: 100, color: 'green', speedX: 120},
        {x: 0, y: 200, width: 100, height: 100, color: 'blue',  speedX: 180},
    ];
    this.lastAnimationTime = Date.now();
};

Animation.prototype.update = function () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    let currentAnimationTime = Date.now();
    let animationTimeDelta = (currentAnimationTime - (this.lastAnimationTime || Date.now())) / 1000;

    this.lastAnimationTime = currentAnimationTime;
    this.rectangles.forEach(rectangle => {
        ctx.fillStyle = rectangle.color;
        ctx.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        rectangle.x += rectangle.speedX * animationTimeDelta;
    });

    window.requestAnimationFrame(this.update.bind(this));
};

let animation = new Animation();
animation.update();
