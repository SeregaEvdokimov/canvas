let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
let width = 500, height = 600;

let rect = [];
let mouse = {
    x: 0,
    y: 0
};


canvas.width = width;
canvas.height = height;
canvas.style.background = 'black';

ctx.strokeStyle = 'white';
ctx.lineWidth = 1;


let clear = function() {
  ctx.clearRect(0, 0, width, height);
};

let fillRect = function(x, y, w, h) {
    ctx.fillRect(x, y, w, h);
};

let strokeRect = function(x, y, w, h) {
    ctx.strokeRect(x, y, w, h);
};


let Rect = function(x, y ,w, h) {
  this.x = x;
  this.y = y;
  this.w = w;
  this.h = h;

  this.dx = 0;
  this.dy = 0;

  this.max = 10;
  this.dif = 0.1;

  this.fall = true;
};

Rect.prototype.draw = function() {
  fillRect(this.x, this.y, this.w, this.h);
};

Rect.prototype.move = function() {
    this.x += this.dx;
    this.y += this.dy;
};

Rect.prototype.grav = function() {
    if(!this.fall) return;

    if(this.y + this.h / 1.5 > height - 200) {
        this.max = 2;
        this.dif = 0.01;
    }

    if(this.dy > this.max) {
        this.dy = this.dy / this.max;
    }


    this.dy += this.dy <= this.max ? this.dif : 0;

    if(this.y + this.h >= height) {
        this.y = height - this.h;
        this.dy /= 2;
        this.dy *= -1;
    }

    if(Math.abs(this.dy) < this.dif * 2 && this.y + this.h >= height) {
        this.fall = false;
        this.dy = 0;
    }
};


setInterval(function() {
   clear();

   ctx.globalAlpha = 1;
   ctx.fillStyle = 'yellow';
    rect.forEach(function(item) {
        item.grav();
        item.move();
        item.draw();
    });

    strokeRect(mouse.x - 15, mouse.y - 15, 30, 30);

    ctx.globalAlpha = 0.3;
    ctx.fillStyle = '#8bdbff';
    fillRect(0, height - 200, width, height);
}, 1000 / 60);


window.addEventListener('mousemove', function(event) {
    mouse.x = event.pageX - 10;
    mouse.y = event.pageY - 10;
});

canvas.addEventListener('click', function() {
    rect.push(new Rect(mouse.x - 15, mouse.y - 15, 30, 30));
});
